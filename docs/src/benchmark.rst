Benchmarking TangoGQL with Locust
*********************************

This document describes the process and results of benchmarking the TangoGQL component of the Taranta Suite using Locust. We are also exploring a new prototype using Ariadne to replace TangoGQL. This document includes the Locust script used, details of the benchmarking process, and the resource usage results.

Basic Script Explanation
========================

The Locust script is designed to benchmark the performance of the TangoGQL service. It includes tasks for authenticating a user, executing GraphQL queries and mutations, and monitoring resource usage (CPU and memory) of the TangoGQL container. The script also establishes a WebSocket connection for GraphQL subscriptions.

- **Authentication:** The script logs in to obtain a token required for subsequent requests.
- **GraphQL Queries and Mutations:** The script performs read (query) and write (mutation) operations on a Tango device.
- **Resource Monitoring:** The script tracks CPU and memory usage of the TangoGQL container, storing the data for analysis.
- **WebSocket Subscription:** The script maintains a subscription to receive real-time updates from the server.

Custom Resource Monitoring Script
=================================

The custom resource monitoring script uses the `psutil` library to capture the CPU and memory usage of all processes associated with the TangoGQL container. This is necessary because TangoGQL and its new Ariadne-based prototype are multithreaded, and we need to capture all resources used by these processes.

- **Process Identification:** The script identifies all processes related to TangoGQL by matching the container name.
- **Resource Logging:** The script logs CPU and memory usage data at regular intervals.

Additionally, the script creates a Flask web application that runs on port 8090 to serve the collected metrics. This web app can be accessed to visualize real-time resource usage.

Locust Configuration
====================

The Locust interface runs on port 8098 and allows for configuring the number of users and ramp-up rate for the load tests. This provides flexibility in testing different load scenarios.

Benchmarking Results
====================

The benchmarking results include total requests per second, response times, number of users, and resource usage (CPU and memory) over time.

.. |TangoGQL| image:: _static/img/TangoGQL.png
   :height: 400 px
   :width: 700 px
   :alt: TangoGQL Benchmark Results

.. |Ariadne| image:: _static/img/Ariadne.png
   :height: 400 px
   :width: 700 px
   :alt: Ariadne Benchmark Results

Resource Usage
==============

During the benchmarking, the following metrics were observed:

- **CPU Usage:** The mean CPU usage, standard deviation, and maximum usage were recorded.
- **Memory Usage:** The mean memory usage, standard deviation, and maximum usage were recorded.

Results are illustrated in the charts below:

.. |TangoGQL_CPU_Memory| image:: _static/img/TangoGQL.png
   :height: 400 px
   :width: 700 px
   :alt: CPU and Memory Usage for TangoGQL

.. |Ariadne_CPU_Memory| image:: _static/img/Ariadne.png
   :height: 400 px
   :width: 700 px
   :alt: CPU and Memory Usage for Ariadne

Results Summary
===============

+--------------------------------+---------------------------+---------------------------+
| Metric                         | TangoGQL                  | Ariadne                   |
+================================+===========================+===========================+
| **Duration (seconds)**         | 119.14                    | 119.13                    |
+--------------------------------+---------------------------+---------------------------+
| **Total Requests/s**           | 148.6                     | 151.3                     |
+--------------------------------+---------------------------+---------------------------+
| **Failures/s**                 | 0%                        | 0%                        |
+--------------------------------+---------------------------+---------------------------+
| **95th Percentile (ms)**       | 15-30                     | 10-25                     |
+--------------------------------+---------------------------+---------------------------+
| **Average Response Time (ms)** | 7-8                       | 5-6                       |
+--------------------------------+---------------------------+---------------------------+
| **Number of Users**            | 60                        | 60                        |
+--------------------------------+---------------------------+---------------------------+
| **CPU Usage (Mean %)**         | 52.70                     | 42.14                     |
+--------------------------------+---------------------------+---------------------------+
| **CPU Usage (Max %)**          | 75.00                     | 63.00                     |
+--------------------------------+---------------------------+---------------------------+
| **Memory Usage (Mean MB)**     | 82.79                     | 83.30                     |
+--------------------------------+---------------------------+---------------------------+
| **Memory Usage (Max MB)**      | 83.35                     | 83.30                     |
+--------------------------------+---------------------------+---------------------------+

Conclusion
==========

Based on the benchmarking results, the new Ariadne prototype demonstrates several improvements over the existing TangoGQL implementation:

- **Performance:** Ariadne shows a slightly higher total request rate (151.3 requests/s) compared to TangoGQL (148.6 requests/s).
- **Response Time:** Ariadne achieves lower average response times (5-6 ms) compared to TangoGQL (7-8 ms), with a more favorable 95th percentile range (10-25 ms vs. 15-30 ms).
- **Resource Efficiency:** Ariadne is more efficient in terms of CPU usage, with a mean of 42.14% compared to TangoGQL's 52.70%. Both implementations have similar memory usage metrics.

These results indicate that Ariadne not only meets but exceeds the performance and efficiency of the current TangoGQL implementation. Additionally, Ariadne's compatibility with Python 3.10+ addresses the compatibility issues with the older TangoGQL library, which relies on Graphene and is no longer maintained.

Given these advantages, transitioning to the Ariadne-based implementation for TangoGQL is recommended. This upgrade will ensure better performance, resource usage, and future-proofing with the latest Python versions.
