
Introduction
=============

Projects that compose Taranta Suite
-----------------------------------

.. list-table:: 
   :widths: 1 1 1 1
   :header-rows: 1

   * - Repository 
     - Docs
     - Purpose
     - Description
   * - https://gitlab.com/tango-controls/web/taranta.git
     - |Taranta_Docs|
     - A tool for creating Dashboards for 
       interacting with the devices 
       within a Tango Control System
     - The tool for developing these dashboards is taranta itself.  

   * - https://gitlab.com/tango-controls/web/tangogql.git
     - |TangoGQL_Docs|
     - Queryable access to a Tango Control System that can be used by the 
       dashboard creation tool
     - Currently, this is a TangoGQL. A GraphQL web server that integrates with the 
       TangoDB services and cancommunicate directly with tango devices.

   * - https://gitlab.com/tango-controls/web/taranta-dashboard.git  
     - |Taranta Dashboard_Docs|
     - Storing and Sharing the configuration of developed dashboards
       between users
     - A MongoDB based dashboard repository for storing taranta dashboard   
       layouts

   * - https://gitlab.com/tango-controls/web/taranta-auth.git 
     - |Taranta Auth_Docs|
     - Authorization and Authentication for the tools 
     - A simple authentication and authorization service for the taranta and 
       TangoGQL tools that can be hooked into a corporate       
       authentication solution 

   * - https://gitlab.com/tango-controls/web/taranta-suite.git
     - -
     - Supporting further development
     - A set of developer scripts and tools used for setting up and developing 
       these related products. 
