
****
Home
****

.. toctree::
  :maxdepth: 1
  :caption: Home
  :hidden:

.. include::    introduction.rst


.. toctree::
    :maxdepth: 1
    :caption: Benchmarking 

    benchmark

Authors
=======

Taranta was initially written by the KITS Group at MAX IV Laboratory. 
Since 2018 also SKA Teams are developing it.

