.PHONY: all run

all : taranta tangogql taranta-auth taranta-dashboard 

taranta :
	git clone https://gitlab.com/tango-controls/web/taranta.git

tangogql :
	git clone https://gitlab.com/tango-controls/web/tangogql.git

tangogql-ariadne :
	git clone https://gitlab.com/tango-controls/incubator/tangogql-ariadne.git

taranta-auth :
	git clone https://gitlab.com/tango-controls/web/taranta-auth.git

taranta-dashboard :
	git clone https://gitlab.com/tango-controls/web/taranta-dashboard.git

run : all
	docker-compose build && docker-compose up --remove-orphans

build-docs:
	docker run --rm -d -v $(PWD):/tmp -w /tmp/docs netresearch/sphinx-buildbox sh -c "make html"

down: 
	docker-compose down --remove-orphans